global.name = 'US East - DEV';

console.log(' Loading...');

var express = require('express'); // Import the Express module
var path = require('path'); // Import the 'path' module (packaged with Node.js)
var app = express(); // Create a new instance of Express

var server = require('http').createServer(app).listen(process.env.PORT || 8080); // Create a Node.js based http server on port 8080
global.io = require('socket.io').listen(server); // Create a Socket.IO server and attach it to the http server
global.Hotel = require('socket.io-hotel');

var googleTrends = require('google-trends-api');

//// Server Config
global.tickrate = 1500;
global.debugmode = false;
global.fakedata = true;
global.fakeresults = [
    { 'December 2003': '40' },
    { 'January 2004': '38' },
    { 'February 2004': '39' },
    { 'March 2004': '38' },
    { 'April 2004': '36' },
    { 'May 2004': '36' },
    { 'June 2004': '39' },
    { 'July 2004': '37' },
    { 'August 2004': '38' },
    { 'September 2004': '47' },
    { 'October 2004': '67' },
    { 'November 2004': '45' },
    { 'December 2004': '36' },
    { 'January 2005': '36' },
    { 'February 2005': '38' },
    { 'March 2005': '37' },
    { 'April 2005': '38' },
    { 'May 2005': '38' },
    { 'June 2005': '39' },
    { 'July 2005': '40' },
    { 'August 2005': '40' },
    { 'September 2005': '49' },
    { 'October 2005': '73' },
    { 'November 2005': '54' },
    { 'December 2005': '42' },
    { 'January 2006': '40' },
    { 'February 2006': '39' },
    { 'March 2006': '39' },
    { 'April 2006': '40' },
    { 'May 2006': '37' },
    { 'June 2006': '41' },
    { 'July 2006': '38' },
    { 'August 2006': '39' },
    { 'September 2006': '46' },
    { 'October 2006': '65' },
    { 'November 2006': '54' },
    { 'December 2006': '39' },
    { 'January 2007': '37' },
    { 'February 2007': '36' },
    { 'March 2007': '37' },
    { 'April 2007': '37' },
    { 'May 2007': '38' },
    { 'June 2007': '40' },
    { 'July 2007': '38' },
    { 'August 2007': '39' },
    { 'September 2007': '45' },
    { 'October 2007': '66' },
    { 'November 2007': '57' },
    { 'December 2007': '46' },
    { 'January 2008': '43' },
    { 'February 2008': '43' },
    { 'March 2008': '41' },
    { 'April 2008': '41' },
    { 'May 2008': '41' },
    { 'June 2008': '42' },
    { 'July 2008': '43' },
    { 'August 2008': '44' },
    { 'September 2008': '52' },
    { 'October 2008': '72' },
    { 'November 2008': '52' },
    { 'December 2008': '44' },
    { 'January 2009': '42' },
    { 'February 2009': '43' },
    { 'March 2009': '43' },
    { 'April 2009': '44' },
    { 'May 2009': '43' },
    { 'June 2009': '45' },
    { 'July 2009': '46' },
    { 'August 2009': '46' },
    { 'September 2009': '53' },
    { 'October 2009': '74' },
    { 'November 2009': '68' },
    { 'December 2009': '51' },
    { 'January 2010': '47' },
    { 'February 2010': '47' },
    { 'March 2010': '46' },
    { 'April 2010': '46' },
    { 'May 2010': '45' },
    { 'June 2010': '49' },
    { 'July 2010': '50' },
    { 'August 2010': '52' },
    { 'September 2010': '59' },
    { 'October 2010': '81' },
    { 'November 2010': '59' },
    { 'December 2010': '51' },
    { 'January 2011': '49' },
    { 'February 2011': '49' },
    { 'March 2011': '50' },
    { 'April 2011': '49' },
    { 'May 2011': '50' },
    { 'June 2011': '53' },
    { 'July 2011': '53' },
    { 'August 2011': '54' },
    { 'September 2011': '64' },
    { 'October 2011': '87' },
    { 'November 2011': '67' },
    { 'December 2011': '55' },
    { 'January 2012': '52' },
    { 'February 2012': '56' },
    { 'March 2012': '74' },
    { 'April 2012': '70' },
    { 'May 2012': '58' },
    { 'June 2012': '63' },
    { 'July 2012': '58' },
    { 'August 2012': '61' },
    { 'September 2012': '64' },
    { 'October 2012': '88' },
    { 'November 2012': '66' },
    { 'December 2012': '55' },
    { 'January 2013': '54' },
    { 'February 2013': '58' },
    { 'March 2013': '52' },
    { 'April 2013': '53' },
    { 'May 2013': '53' },
    { 'June 2013': '54' },
    { 'July 2013': '56' },
    { 'August 2013': '59' },
    { 'September 2013': '66' },
    { 'October 2013': '91' },
    { 'November 2013': '65' },
    { 'December 2013': '54' },
    { 'January 2014': '53' },
    { 'February 2014': '54' },
    { 'March 2014': '52' },
    { 'April 2014': '52' },
    { 'May 2014': '52' },
    { 'June 2014': '54' },
    { 'July 2014': '55' },
    { 'August 2014': '55' },
    { 'September 2014': '63' },
    { 'October 2014': '91' },
    { 'November 2014': '64' },
    { 'December 2014': '55' },
    { 'January 2015': '52' },
    { 'February 2015': '55' },
    { 'March 2015': '55' },
    { 'April 2015': '52' },
    { 'May 2015': '54' },
    { 'June 2015': '55' },
    { 'July 2015': '54' },
    { 'August 2015': '58' },
    { 'September 2015': '67' },
    { 'October 2015': '100' },
    { 'November 2015': '76' },
    { 'December 2015': '59' },
    { 'January 2016': '53' },
    { 'February 2016': '57' },
    { 'March 2016': '51' },
    { 'April 2016': '52' },
    { 'May 2016': '49' } 
]

global.chartdepth = 36; // how far back the charts go in months

var gameSocket;


// this needs moved
var gamedata = {
    topics: [
        {
            name: 'topicname',
            cards: [['', '0'],
                    ['1', ''],
                    ['Use Code:', ''],
                    ['3', ''],
                    ['', '4'],
                    ['5', '']]
        }
    ]
}

var gamerooms = [
    {
        id:'sdfsdfefsdfxc',
        name:'testroom',
        owner:'thegreatj',
        playercount:'2'
    },
    {
        id:'sdfsdfsdfxc',
        name:'testroom1',
        owner:'thegreatj',
        playercount:'20'
    },
    {
        id:'sdfsdsdfxc',
        name:'testroom2',
        owner:'thegreatj',
        playercount:'1'
    },
    {
        id:'sdfsefsdfxc',
        name:'testroom3',
        owner:'thegreatj',
        playercount:'2'
    }
]

// Start Server
initGame();
 
 // Initialize Game
function initGame(){
    
    var hotel = new Hotel(io.sockets.adapter);
    
    // Player Events
	io.on('connection', onClientConnection);
	
	console.log(' Server Started');
    console.log(' NAME: '+name);
    console.log(' TICKRATE: '+tickrate);
    console.log(' DEBUG: '+debugmode);
    
    // update
    var tick = function() {
        setTimeout(tick, tickrate);
//        hotel.listRooms(function(rooms) {
//            console.log(Object.keys(rooms))
//        })
    }
    tick();
}

//// Player Functions

// Joining
function onClientConnection(client) {
	console.log('clientid: ' + client.id + ' connected');
    client.room = '###';
    client.join('###');
	client.on('disconnect', onClientDisconnect);
	client.on('getRoomList', onGetRoomList);
	client.on('submitkeyword', onSubmitKeyword);
}
// Leaving
function onClientDisconnect() {
	console.log(this.id + ' disconnected');
    this.leave(this.room);
}
function onGetRoomList() {
    this.emit('roomlist', gamerooms);
}

// submit keyword
function onSubmitKeyword(data) {
    if (fakedata) {
        var resultdata = {k: 'fakedata', d: fakeresults};
    } else {
        googleTrends.trendData([data.k])
        .then(function(results){
            var resultdata = {k: data.k, d: results[0]};
        }).catch(function(err){
            console.log(err);
        });
    }
    
    resultdata.d.splice(0, resultdata.d.length-chartdepth)
    
    var sum = 0
    for (var i=0; i<resultdata.d.length; i++) {
        var cur = resultdata.d[i]
        var key = Object.keys(cur)[0]
        sum += Number(cur[key])
    }
    resultdata.a = sum/chartdepth
    console.log(resultdata);
    io.emit('results', resultdata);
}






