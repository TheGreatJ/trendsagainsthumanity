console.log(' Loading Index Server...');

var express = require('express'); // Import the Express module
var path = require('path'); // Import the 'path' module (packaged with Node.js)
var app = express(); // Create a new instance of Express

app.use(express.static(path.join(__dirname,'public'))); //  static html, js, css, and image files from the 'public' directory

var server = require('http').createServer(app).listen(process.env.PORT || 80); // Create a Node.js based http server on port 8080
global.io = require('socket.io').listen(server); // Create a Socket.IO server and attach it to the http server

//// Server Config
global.tickrate = 500;
global.debugmode = false;

var gameSocket;

// Start Server
init();
 
 // Initialize Game
function init(){
    
    // Player Events
	io.on('connection', onClientConnection);
	
	console.log(' Index Server Started');
    
    // update
    var tick = function() {
        setTimeout(tick, tickrate);
    }
    tick();
}

// Joining
function onClientConnection(client) {
	console.log('clientid: ' + client.id + ' connected');    
    
	client.on('disconnect', onClientDisconnect);
    //io.emit('roomsync', roomdata);
}
// Leaving
function onClientDisconnect() {
	console.log(this.id + ' connected');
}


