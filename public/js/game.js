var socket;

var servers = [
    {
        id: 'USE1',
        name:'US East - DEV',
        roomcount:'0',
        ip: '71.61.84.167',
        port: '8080'
    }
]

var curserver;
var curserverid;
var curserverindex;


//// Menu System

// scrollbar settings
var scrollpara = {
    wheelSpeed: 0.25,
    wheelPropagation: true,
    minScrollbarLength: 20
}

var menu = {};
menu.home = {}
menu.join = {}
menu.join.onshow = function(e) {
    
    e.classList.add('servers');
    
    var serverlist = e.querySelector("#content>#serverlist")
    var roomlist = e.querySelector("#content>#roomlist")
    
    for (var i=0; i < servers.length; i++) {
        var cur = servers[i]
        var a = document.createElement('a');
        a.id = cur.id+'-'+i;
        a.classList = 'collection-item';
        a.setAttribute('data-index', i);
        a.setAttribute('onclick', 'menu.join.serverselect(this)');
        a.innerHTML = cur.name;
        var span = document.createElement('span');
        span.id = 'roomcount';
        span.classList = 'badge';
        span.innerHTML = cur.roomcount;
        a.appendChild(span);
        serverlist.querySelector("div.collection").appendChild(a);
    }
    
    var spin = e.querySelector("#spinner");
    Ps.initialize(serverlist, scrollpara);
    Ps.initialize(roomlist, scrollpara);
    spin.classList = 'loaded';
    e.querySelector("#content").classList = 'loaded';
    
}
menu.join.serverselect = function(e) {
    var menu = document.querySelector("div#join")
    menu.classList.remove('servers');
    menu.classList.add('rooms');
    
    var list = menu.querySelector("#content>#serverlist")
    if (curserverid) {
        var cur = list.querySelector('a#'+curserverid);
        cur.classList.remove('selected');
    }
    e.classList.add('selected');
    curserverid = e.id;
    curserverindex = e.getAttribute('data-index');
    curserver = servers[curserverindex];
    
    // scroll into view
    var s = list.scrollTop
    if (list.clientHeight < e.offsetTop+e.clientHeight-s) {
        list.scrollTop = (e.offsetTop+e.clientHeight)-list.clientHeight;
        Ps.update(list);
    } else if (s > e.offsetTop-s) {
        list.scrollTop = e.offsetTop;
        Ps.update(list);
    }
    
    if (socket) {
        socket.disconnect();
    }
    socket = io.connect('http://'+curserver.ip+':'+curserver.port);
    setEventHandlers();
}
menu.join.onroomlist = function(data) {
    var menu = document.querySelector("div#join")
    var roomlist = menu.querySelector("#content>#roomlist>div.collection")
    
    roomlist.innerHTML = ''
    
    for (var i=0; i < data.length; i++) {
        var cur = data[i]
        
        var a = document.createElement('a');
        a.id = 'room-'+i;
        a.classList = 'collection-item';
        a.setAttribute('data-roomid', cur.id);
        a.setAttribute('onclick', 'menu.join.roomselect(this)');
        a.innerHTML = cur.name;
        
        var owner = document.createElement('span');
        owner.id = 'owner';
        owner.classList = 'owner';
        owner.innerHTML = cur.owner;
        a.appendChild(owner);
        
        var playericon = document.createElement('i');
        playericon.classList = 'material-icons playericon';
        playericon.innerHTML = 'perm_identity';
        a.appendChild(playericon);
        
        var playercount = document.createElement('span');
        playercount.classList = 'playercount';
        playercount.innerHTML = cur.playercount;
        a.appendChild(playercount);
       
        roomlist.appendChild(a);
    }
    
}
menu.host = {}
menu.roomlist = {}
menu.host = {}


function menushow(id) {
    var div = document.querySelector('#' + id);
    div.classList = 'show';
    if (menu[id].onshow) {
        menu[id].onshow(div);
    }
}
function menuhide(id) {
    
    var div = document.querySelector('#' + id);
    div.classList = 'hide';
    if (menu[id].onhide) {
        menu[id].onhide(div);
    }
}
function menutran(from, to) {
    var fdiv = document.querySelector('#' + from);
    var tdiv = document.querySelector('#' + to);
    menuhide(from)
    menushow(to)
}
function menurippler(x, y, color) {
    
    var radius = 0
    var w = window.innerWidth;
    var h = window.innerHeight;
    var a, b;
    
    if ( x < w*0.5 ) {a = w - x} 
    else {a = x}
    if ( y < h*0.5 ) {b = h - y} 
    else {b = y}
    
    radius = Math.sqrt((a*a)+(b*b))*2
    
    if (!(!document.querySelector('style[title="rippler"]'))) {
        document.getElementsByTagName("head")[0].removeChild(document.querySelector('style[title="rippler"]'));
    }
    var cssAnimation = document.createElement('style');
    cssAnimation.type = 'text/css';
    cssAnimation.title = 'rippler';
    var rules = document.createTextNode('@-webkit-keyframes rippler {'+
    'from { width:0px; height:0px;}'+
    'to { width:'+radius+'px; height:'+radius+'px;}'+
    '}');
    cssAnimation.appendChild(rules);
    document.getElementsByTagName("head")[0].appendChild(cssAnimation);
    
    var rplr = document.createElement('div');
    rplr.classList = 'rippler';
    rplr.style.left = x+'px';
    rplr.style.top = y+'px';
    rplr.style.backgroundColor = color;
    
    document.body.appendChild(rplr);
    
    rplr.addEventListener("animationend", function(e){
        document.body.style.backgroundColor = this.style.backgroundColor
        document.body.removeChild(this);
    }, false);
}

window.onload = function(){
    menuhide('home')
    menushow('home')
    $('.btn-ripple').on("click",function(e){
        var btn = $(this);
        menurippler(e.clientX, e.clientY, btn.css("background-color"));
        btn.addClass('ripple');
    })
}

var game = $('game');


// connect to io server

google.charts.load('current', {
    packages: ['corechart', 'line']
});

var keywordinput = {};
keywordinput.last = '';

var roomdata = {
    curtopic: '',
    curcard: [],
    players: []
}

keywordinput.oninput = function (e) {
    var reg = /^[a-zA-Z0-9_]{0,16}$/; //accepted characters
    // only allow reg to be entered
    if (reg.test(e.value)) {
        keywordinput.last = e.value
    } else {
        e.value = keywordinput.last
    }

    // expand input with the text
    var tw = $('#textwidth')
    tw.html(e.value)
    var width = tw.width() + 8
    if (width > 80) {
        e.style.width = width + 'px';
    } else {
        e.style.width = '80px';
    }
}
// submit keyword
keywordinput.submit = function () {
        var e = document.querySelector('#keywordinput')
        var reg = /^[a-zA-Z0-9_]{1,16}$/; //accepted characters
        if (reg.test(e.value)) {
            socket.emit('submitkeyword', {
                k: e.value
            });
        }
    }
    // enter key to submit keyword
$(document).on('keyup', function (event) {
    if (event.keyCode == 13) {
        keywordinput.submit()
    }
});

// enable Socket Handlers
var setEventHandlers = function () {
    // Socket connection successful
    socket.on('connect', onSocketConnected);
    socket.on('disconnect', onSocketDisconnect);
    socket.on('roomlist', onRoomList);
    socket.on('roomsync', onRoomSync);
    socket.on('newcard', onNewCard);
    socket.on('results', onResults);
}

// Socket connected
function onSocketConnected() {
    console.log('Connected to server');
    socket.emit('getRoomList');
}
// Socket disconnected
function onSocketDisconnect() {
    console.log('Disconnected from server');
}
function onRoomList(data) {
    menu.join.onroomlist(data);
}

function onNewCard() {

}

function onRoomSync(data) {
    roomdata = data;
}

function onResults(data) {
    console.log('hey');
    console.log(data);
    graph('chart1', data.k, data.d)
}

var chart
var chartdata
var options = {
    hAxis: {
        title: 'Time'
    },
    vAxis: {
        title: 'Popularity'
    },
};

function graph(id, keyword, data) {
    chart = new google.visualization.LineChart(document.getElementById(id));
    chartdata = new google.visualization.DataTable();
    chartdata.addColumn('string', 'Time');
    chartdata.addColumn('number', '');

    var rowdata = []
    for (var i = 0; i < data.length; i++) {
        var cur = data[i]
        var key = Object.keys(cur)[0]
        var newrow = [key, Number(cur[key])]
        rowdata.push(newrow)
    }

    chartdata.addRows(rowdata)

    chart.draw(chartdata, options);
}